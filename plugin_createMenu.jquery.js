(function ($, window) {
    var menuAPI = {
        createTemplate: function (obj) {
            var temp1 =  $.fn.createMenu.options.template1,
                temp2 =  $.fn.createMenu.options.template2,
                event,
                callbackFn;

            if ( ~(obj.linkOrAction.indexOf('http')) ) {
                return temp1.replace(/{{linkOrAction}}/ig, obj.linkOrAction)
                    .replace(/{{title}}/ig, obj.title)
                    .replace(/{{bgColor}}/ig, obj.bgColor);
            }
            else {
                event = obj.linkOrAction.split('#')[0];
                callbackFn = obj.linkOrAction.split('#')[1];
                return temp2.replace(/{{title}}/ig, obj.title)
                                .replace(/{{event}}/ig, event)
                                .replace(/{{callbackFn}}/ig, callbackFn)
                                .replace(/{{bgColor}}/ig, obj.bgColor);
            }
        },

        addItemsToDropdown: function (item, elem) {
            var $additionalNav = elem.find('.additional-nav'),
                ul;

            //if additional-nav already exist, doesn't create it
            if ( $additionalNav.length ) {
                return $additionalNav.append(item);
            }
            else {
                ul = "<ul class='additional-nav hide'></ul>";
                return elem.append(ul);
            }
        },

        createBtnForNav: function (elem) {
            var btn = "<button class='additional-nav-btn'>Click</button>";
            return elem.append(btn);
        },

        calcWidthOfElems: function (arr) {
            var res = 0;
            arr.each(function (i, elem) {
                res += $(elem).width();
            });
            return res;
        }

    };

    $.fn.createMenu = function (data) {
        // each element from the selected collection
        return this.each(function () {
            var $container = $(this),
                ul = "<ul class='clearfix main-nav'>",
                $ul,
                menu = Object.create(menuAPI),
                item,
                navBtn,
                $additionalNav,
                $mainNav;

            //check on a repeated call with the same element
            if ( $container.find('.main-nav').length ) {
                return;
            }

            //check if function called without arguments
            if ( !data ) {
                $.ajax({
                    dataType: "json",
                    url: 'data.json',
                    success: function (arg) {
                        $container.createMenu(arg);
                    },
                    error: function () {
                        console.log("fail");
                    }
                });
                return;
            }

            // each object that come from the json
            $(data).each(function (index, element) {
                item = menu.createTemplate(element);
                ul += item;
            });
            ul += "</ul>";

            $container.append(ul);
            //get reference to the <ul> inside $container
            $ul = $container.children().last();

            while ( $ul.height() > $ul.children().last().height() ) {

                $additionalNav = $container.find('.additional-nav');
                $mainNav = $container.find('.main-nav');

                if ( !($container.find('.additional-nav-btn').length) ) {
                    menu.createBtnForNav($container);
                    navBtn = $container.find('.additional-nav-btn');

                    //show additional-nav, if btn has been clicked
                    navBtn.on('click', function () {

                        if ( $additionalNav.hasClass('show') ) {
                            $additionalNav.removeClass('show').addClass('hide');
                        }
                        else {
                            $additionalNav.removeClass('hide').addClass('show');
                        }
                    });

                    //when DOM loaded check, if items can't be placed in one line in main-nav (not enough width)
                    $(window).ready(function () {
                        var $li;

                        while ( $mainNav.height() > $mainNav.children().last().height() ) {
                            $li = $mainNav.children().last();
                            menu.addItemsToDropdown($li, $container)
                        }
                    });

                    // when window resize check, if items can't be placed in one line (not enough width), those items will be hide to dropdown
                    //    or if width of main-nav enough, they will be placed back in the main-nav from dropdown
                    var prevMainNavWidth;  //variable to know that window width reduced or increases
                    $(window).on('resize', function(){
                        var $li;

                        if ( prevMainNavWidth < $mainNav.width() ) {

                            if ( ($mainNav.width() - menu.calcWidthOfElems($mainNav.children())) >=  $additionalNav.children().last().width() ) {
                                $li = $additionalNav.children().last();
                                $mainNav.append($li);
                            }
                        }
                        else if ( prevMainNavWidth > $mainNav.width() ){

                            if ( ($mainNav.width() - menu.calcWidthOfElems($mainNav.children())) <  $additionalNav.children().last().width() ) {
                                $li = $mainNav.children().last();
                                menu.addItemsToDropdown($li, $container);
                            }
                        }

                        prevMainNavWidth = $mainNav.width();
                    });
                }

                $li = $ul.children().last();
                menu.addItemsToDropdown($li, $container);
            }
        })
    };

    //possibility change templates, if it needed
    $.fn.createMenu.options = {
        template1: "<li><a href={{linkOrAction}} style=background-color:{{bgColor}}>{{title}}</a></li>",
        template2: "<li><span style=background-color:{{bgColor}} on{{event}}={{callbackFn}}" + "(event)" + ">{{title}}</span></li>"
    }
})(jQuery, window);